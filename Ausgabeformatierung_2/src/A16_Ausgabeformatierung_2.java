
public class A16_Ausgabeformatierung_2 {

	public static void main(String[] args) {
		
		String f = "0!";
		
		System.out.printf( "%-5s", f );
		
		
		
		String s = "= 1 * 2 * 3 * 4 * 5 = 120";
		
		System.out.printf("%.1s\n",s);
		
		
		
		String f1 = "1!";
		
		System.out.printf( "%-5s", f1 );
		
		System.out.printf("%.3s\n",s);

		
		
		String f2 = "2!";
		
		System.out.printf( "%-5s", f2 );
		
		System.out.printf("%.8s\n",s);
		
		
		
		String f3 = "3!";
		
		System.out.printf( "%-5s", f3 );
		
		System.out.printf("%.12s\n",s);
		
		
		
		
		String f4 = "4!";
		
		System.out.printf( "%-5s", f4 );
		
		System.out.printf("%.16s\n",s);
		
		
		String f5 = "5!";
		
		System.out.printf( "%-5s", f5 );
		
		System.out.printf("%.20s\n",s);
		
		
		
		
	}
	
	
	
}

