package Konsolenausgabeformatierung;

public class Aufgabe1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.print("Aufgabe 1. \r");
		
		System.out.println("\r Das ist ein Beispielsatz. Ein Beispielsatz ist das. \r");
		
		System.out.print("\rAufgabe 2.");
		
		System.out.println("\r \r Das ist ein \"Beispielsatz\". \n Ein Beispielsatz ist das.");
		
		System.out.print("\r Aufgabe 3.");
		
		double d1 = 22.4234234;
		
		double d2 = 111.2222; 
				
		double d3 = 4.0;
		
		double d4 = 1000000.551;
				
		double d5 = 97.34;
		
		System.out.printf("\r \r %.2f", d1);
		
		System.out.printf("\r \r %.2f", d2);
		
		System.out.printf("\r \r %.2f", d3);
		
		System.out.printf("\r \r %.2f",d4);
		
		System.out.printf("\r \r %.2f", d5);
		
		
		// Der Unterschied zwischen print() und println besteht darin, dass println() zus�tzlich einen Zeilenumbruch nach der Ausgabe einf�gt.
		

	}
		
		
	public static void aufgabe1_2(String[] args) {
			
		System.out.print("Das ist ein Beispielsatz. Ein Beispielsatz ist das.");
		
	}
		
}