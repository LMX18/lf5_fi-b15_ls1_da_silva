package methoden;

public class Volume {
	
	public static void main(String[] args) {
		wurfel(20);
		quadrat(20,5,2);
		pyramide(9,20);
		kugel(6);
	}
	
public static double wurfel(double a_cm) {
		
		double resultat = a_cm*a_cm*a_cm;
		
		System.out.printf("Ein einseitige w�rfel von %.2f hat ein volum von %f\n",a_cm,resultat);
				
		return resultat;
				
	}

public static double quadrat(double a_cm, double b_cm, double c_cm) {
	
	double resultat = a_cm*b_cm*c_cm;
	
	System.out.printf("ein quadrat mit %.2f hoch, %.2f breite und %.2f tiefe, besteht einer volumen von  %f\n",a_cm,b_cm,c_cm,resultat);
			
	return resultat;
			
}

public static double pyramide(double a, double h) {
	
	double resultat = a*a*(h/3);
	
	System.out.printf("Eine pyramide, mit eine seite von %.2f und die h�he von %.2f besthet aus ein volumen von %f\n",a,h,resultat);
	
	return resultat;
			
}

public static double kugel(double r) {
	
	double resultat = 4/3*r*r*r*3.14;
	
	System.out.printf("Eine einseitige kugel von %.2f hat ein volumen von %f\n",r,resultat);
	
	return resultat;
			
}

}


//Scanner myScanner = new Scanner(System.in);