package Aufgabe3_3;

import java.util.Scanner;

class aufgabe3_3
{
    public static void main(String[] args)
    {
    	
    	
    	
    	double zuZahlenderBetrag;
    	double eingezahlterGesamtbetrag;
       	double eingeworfeneM�nze;
       	double r�ckgabebetrag;

       	zuZahlenderBetrag = fahrkartenbestellungErfassen();
       
       	// Geldeinwurf
       	// -----------
       	r�ckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);

       	// Fahrscheinausgabe
       	// -----------------
       	fahrkartenAusgeben();

       	// R�ckgeldberechnung und -Ausgabe
       	// -------------------------------
       	rueckgeldAusgeben(r�ckgabebetrag);
       	
       	System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                "vor Fahrtantritt entwerten zu lassen!\n"+
                "Wir w�nschen Ihnen eine gute Fahrt.");
       
    }
    public static double fahrkartenBezahlen(double zuZahlen) {
        double eingezahlterGesamtbetrag = 0.0;
    	Scanner tastatur = new Scanner(System.in);

        while(eingezahlterGesamtbetrag < zuZahlen)
        {
     	   	System.out.printf("Noch zu zahlen: %.2f Euro ",(zuZahlen - eingezahlterGesamtbetrag));
     	   	System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
     	   	double eingeworfeneM�nze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneM�nze;
        }
    	
    	return eingezahlterGesamtbetrag - zuZahlen;
    }
    public static double fahrkartenbestellungErfassen() {
    	Scanner tastatur = new Scanner(System.in);
    	System.out.print("Ticketpreis (Euro): ");
        double ticketPreis = tastatur.nextDouble();//der Preis sollte nachkomma stellen haben, deswegen brauchen wir double.

        System.out.print("Anzahl der Tickets: ");
        int anzahlDerTickets = tastatur.nextInt();//int ,weil mann kein halben ticket gebrauchen kann.
        return ticketPreis * anzahlDerTickets;
    }
    public static void fahrkartenAusgeben() {// keine parameter weil es keine braucht
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			e.printStackTrace();
 			}
        }
        System.out.println("\n\n");
    }
    public static void rueckgeldAusgeben(double r�ckgabebetrag) {
    	if(r�ckgabebetrag > 0.0)
       	{
       		System.out.println("Der R�ckgabebetrag in H�he von " + r�ckgabebetrag + " EURO");
       		System.out.println("wird in folgenden M�nzen ausgezahlt:");

       		while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
       		{
       			System.out.println("2 EURO");
       			r�ckgabebetrag -= 2.0;
       		}
       		while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
       		{
       			System.out.println("1 EURO");
       			r�ckgabebetrag -= 1.0;
       		}
       		while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
       		{
       			System.out.println("50 CENT");
       			r�ckgabebetrag -= 0.5;
       		}
       		while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
       		{
       			System.out.println("20 CENT");
       			r�ckgabebetrag -= 0.2;
       		}
       		while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
       		{
       			System.out.println("10 CENT");
       			r�ckgabebetrag -= 0.1;
       		}
   				while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
       			{
   				System.out.println("5 CENT");
       			r�ckgabebetrag -= 0.05;
       			}
       	}
    }
}