package aufgabe_A4_4;

import java.util.Scanner;

public class Aufgabe_A4_3 {
	
	public static void main(String[] args) {

		// (E) "Eingabe"
		// Werte fur x und y festlegen:
		// ===========================
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Von wie vielen Zahlen soll der Mittelwert berechnet werden?");
		int anzahl = tastatur.nextInt();
		double summe = 0;
		int zaehlschleife = 0;

		// (V) Verarbeitung
		// Mittelwert von x und y berechnen:
		// ================================
		while (zaehlschleife < anzahl) {
			System.out.println("Bitte gebe eine Zahl fur die Mittelwertberechnung ein: ");
			double zahl = tastatur.nextDouble();
			zaehlschleife = zaehlschleife + 1;
			summe = summe + zahl;
		}
		double mittelwert = summe /anzahl;

		// (A) Ausgabe
		// Ergebnis auf der Konsole ausgeben:
		// =================================
		System.out.print("Der Mittelwert lautet: " + mittelwert);
		
		

	
	}

}
