package aufgabe_3;

public class Temperaturtabelle {
	
	public static void main(String[] args) {
		
		String aufgabe3 = "Aufgabe 3";
		
		String fahreheiten = "Fahrenheit";
		String celcius = "Celsius";
		String a1 = "---------------------------------------";
		String a2 = "_______________________________________";
		System.out.printf("\n %s", a2);
		System.out.printf("\n|%24s               |", aufgabe3, "|%25s");
		System.out.printf("\n|%s|", a2);
		System.out.printf("\n|   %-15s|%16s    |",fahreheiten, celcius);
		System.out.printf("\n|%s|" ,a1);
		System.out.printf("\n|       %-11d|%16.5f    |",-20,-28.8889);
		System.out.printf("\n|%s|" ,a1);
		System.out.printf("\n|       %-11d|%16.5f    |",-10,-23.3333);
		System.out.printf("\n|%s|" ,a1);
		System.out.printf("\n|        %-10d|%16.5f    |",-0,-17.7778);
		System.out.printf("\n|%s|" ,a1);
		System.out.printf("\n|        %-10d|%16.5f    |",20,6.6667);
		System.out.printf("\n|%s|" ,a1);
		System.out.printf("\n|        %-10d|%16.5f    |",30,-1.1111);
		System.out.printf("\n|%s|" ,a2);
		
	}

}
